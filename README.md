# Python MongoDB first encounter

## Prerequisites

- [ ] docker-compose
- [ ] python 3.10.5

## Getting started

### Virtual environment

1. create a virtual environment with python 3.10.5, and activate it
2. install the requirements: `pip install -r requirements.txt`

### MongoDB - Docker

To start the MongoDB via docker: `docker-compose up`.
You can now browse to [localhost:8081](http://localhost:8081) to view your MongoDB environment

### Python

Simply run either `python main.py` or `./main.py` to insert, find, and delete documents.
