#!/usr/bin/env python
from bson.objectid import ObjectId
from dataclasses import dataclass, asdict
from datetime import datetime
from pprint import pprint
from pymongo import MongoClient
from pymongo.collection import Collection as MongoCollection
from pymongo.database import Database as MongoDatabase


@dataclass
class Post:
    author: str = 'unknown'
    text: str = ''
    tags: list = None
    date: datetime = datetime.utcnow()

    def __post_init__(self):
        if self.tags is None:
            self.tags = []

    def as_dict(self) -> dict[str, any]:
        return {k: str(v) for k, v in asdict(self).items()}


if __name__ == '__main__':
    client: MongoClient = MongoClient(host='mongodb://root:example@localhost:27017/')

    try:
        # create new post
        post: Post = Post(
            author='Nico',
            text='Some sample text.',
            tags=['mongodb', 'python', 'pymongo'],
        )

        # set up default database and collection (table)
        default_db: MongoDatabase = client.local
        posts_collection: MongoCollection = default_db.posts

        # insert the post and get its ID
        post_id: ObjectId = posts_collection.insert_one(post.as_dict()).inserted_id
        # get the previously inserted post by ID
        inserted_post: dict[str, any] = posts_collection.find_one({'_id': post_id})
        pprint({'inserted post': inserted_post})

        # create some more posts
        multiple_posts: list[dict[str, any]] = [
            Post(author='Nico').as_dict(),
            Post(tags=['some', 'tag']).as_dict(),
            Post(text='some text').as_dict(),
        ]

        # insert multiple posts at once and get their IDs
        multi_insert_ids: list[ObjectId, ...] = posts_collection.insert_many(multiple_posts).inserted_ids
        pprint({'inserted post ids': multi_insert_ids})

        # find all posts for single author
        print('all posts for single author')
        for post in posts_collection.find({'author': 'Nico'}):
            pprint(post)

        # delete single post
        deleted_post_count = posts_collection.delete_one({'_id': post_id}).deleted_count
        print(f'deleted {deleted_post_count} posts (single delete).')

        # delete many posts
        deleted_post_count = posts_collection.delete_many({'author': 'Nico'}).deleted_count
        print(f'deleted {deleted_post_count} posts (multiple delete).')
    finally:
        client.close()
